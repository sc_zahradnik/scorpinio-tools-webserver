@echo off

set batchfile=""

set bitVersion=86
cmd /c %~dp0commands/getBitVersion.bat>tmp.txt
set /P bitVersion= < tmp.txt

cmd /c %~dp0commands/php/%bitVersion%/php.exe %~dp0commands/cmds.php apache %*>tmp.txt
set /P batchfile= < tmp.txt

if "%1" EQU "install" cmd /c %batchfile% & goto :end
if "%1" EQU "uninstall" cmd /c %batchfile% & goto :end
if "%1" EQU "start" cmd /c %batchfile% & goto :end
if "%1" EQU "restart" cmd /c %batchfile% & goto :end
if "%1" EQU "stop" cmd /c %batchfile% & goto :end

type tmp.txt
rm tmp.txt

:end