@echo off

set tmp=0
set flag=%1

set name=Apache2.4.46x64

IF "%flag%" == "-t" (
	set name=%name%_t
)

sc queryex type=service state=all | cmd /c %~dp0../../../../commands/grep.exe -oP -m1 "%name%\s">tmp.txt
set /p tmp= < tmp.txt
rm tmp.txt

IF "%flag%" EQU "-t" (
	echo Starting temporary service %name%

	rem terporary service
	rem system will remove it after stoping service
	rem runas /profile /user:Martin "cmd /c %~dp0bin/httpd.exe -k start -n %name%"
	cmd /c %~dp0install.bat -t
	cmd /c %~dp0bin/httpd.exe -k start -n %name%
	cmd /c %~dp0uninstall.bat -t
) ELSE IF %tmp% EQU %name% (
	echo Starting service %name%
	rem runas /profile /user:Martin "cmd /c %~dp0bin/httpd.exe -k start -n %name%"
	cmd /c %~dp0bin/httpd.exe -k start -n %name%
) else (
	echo Starting %name%
	%~dp0bin/httpd.exe %*
)
