<?php
namespace WebServer;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

use WebServer\classes\PhpExeFilterIterator;

final class Php extends Commands{
	const REGEX_PHP_VERSION_PATH = "/php[\/\\\\](\d{1,3}\.\d{1,3})[\/\\\\](\d{1,3})[\/\\\\]x(64|86)[\/\\\\](n?ts)/i";
	const REGEX_PHP_FULLVERSION = "/((\d{1,3})\.(\d{1,3}))\.(\d{1,3})x(64|86)-(n?ts)/i";

	public function resolve(string $command = null, array $params = []){
		$command = $command ?? "php";
		return parent::resolve($command, [$params]);
	}

	public function getVersionFromPaths(array $paths): array{
		$versions = [];
		foreach ($paths as $path) {
			$versions[] = $this->getVersionFromPath($path);
		}
		return $versions;
	}
	public function getVersionFromPath(string $path): ?array{
		if (preg_match(self::REGEX_PHP_VERSION_PATH, $path, $matches)) {
			return [
				'version'    => $matches[1],
				'subversion' => $matches[2],
				'bitversion' => $matches[3],
				'threadsafe' => ($matches[4]==="ts"),
			];
		}
		return null;
	}

	public function getFullversion($version, $subversion, $bitversion = null, $threadsafe = true){
		$bitversion = $bitversion ?? $this->getSystemBitVersion();

		return $version.".".$subversion."x".$bitversion."-".($threadsafe?"ts":"nts");
	}
	public function parseFullversion(string $version){
		if (preg_match(self::REGEX_PHP_FULLVERSION, $version, $matches)) {
			return [
				'version'    => $matches[1],
				'subversion' => $matches[4],
				'bitversion' => $matches[5],
				'threadsafe' => ($matches[6]==="ts"),
			];
		}
		return null;
	}

	public function getVersionParams(&$params){
		$version = null;
		$subversion = null;
		$bitversion = null;
		$threadsafe = true;

		foreach (["-v" => "version", "-s" => "subversion", "-b" => "bitversion", "-t" => "threadsafe"] as $flag => $variable) {
			if (($index = array_search($flag, $params)) !== false) {
				unset($params[$index]);
				$index++;
				$$variable = $params[$index];
				unset($params[$index]);
				$params = array_values($params);
			}
		}

		if ($threadsafe) {
			$threadsafe = strtolower($threadsafe);

			switch ($threadsafe) {
				case 'false':
				case 'nonthreadsafe':
				case 'nts':
				case '0':
					$threadsafe = false;
				break;
				case 'true':
				case 'threadsafe':
				case 'ts':
				case '1':
				default:
					$threadsafe = true;
				break;
			}
		}

		return [
			$version,
			$subversion,
			$bitversion,
			$threadsafe,
			$params,
		];
	}
	public function getFullversionParam(&$params){
		return $this->getFlagParam($params, "-p");
	}

	public function echoFullversionParam(string $prefix = "\t"){
		echo $prefix."-p = full php version (8.0.1x64-ts)\n";
	}
	public function echoSetOfVersionParams(string $prefix = "\t"){
		echo $prefix."-v = php version (8.0)\n";
		echo $prefix."-s = php subversion (1)\n";
		echo $prefix."-b = php bitversion (64) (not required, detected from system)\n";
		echo $prefix."-t = threadsafe (true|false|ts|nts) (not required, default is true)\n";
	}

	public function getCurrentVersion(): ?array{
		$apache = $this->getApacheCommands();
		$version = $apache->getCurrentPhpVersion();

		return $version;
	}


	//////////
	// List //
	//////////
	// public function prepListParams($params){
	// 	return $params;
	// }

	public function _list(){
		$rdi = new RecursiveDirectoryIterator($this->php);
		$mfi = new PhpExeFilterIterator($rdi);
		$rii = new RecursiveIteratorIterator($mfi);

		$versions = [];
		$rii->rewind();
		foreach ($rii as $file) {
			if ($file->isFile()) {
				$version = $this->getVersionFromPath($file->getPath());
				if ($version) {
					$fullversion = $this->getFullversion(...array_values($version));
					if (!array_key_exists($fullversion, $versions)) {
						$versions[$fullversion] = $version;
					}
				}
			}
		}
		return $versions;
	}
	public function list(){
		$versions = $this->_list();

		echo implode("\n", array_keys($versions));
		echo "\n";
	}

	/////////
	// PHP //
	/////////
	public function _php(){
		$rdi = new RecursiveDirectoryIterator($this->php);
		$mfi = new PhpExeFilterIterator($rdi);
		$rii = new RecursiveIteratorIterator($mfi);

		$versions = [];
		$rii->rewind();
		foreach ($rii as $file) {
			if ($file->isFile()) {
				$version = $this->getVersionFromPath($file->getPath());
				if ($version) {
					$fullversion = $this->getFullversion(...array_values($version));
					if (!array_key_exists($fullversion, $versions)) {
						$versions[$fullversion] = $version;
					}
				}
			}
		}
		return $versions;
	}
	public function php($params = []){
		//todo:(0) detekovat php verzi z param
		$version = $this->getCurrentVersion();

		if (!$version) {
			echo "Something went wront. System is not able to find current PHP version.\n";
			return null;
		}
		$file = $this->php.implode("/", [
			$version['version'],
			$version['subversion'],
			"x".$version['bitversion'],//todo: (0) pozor, kontrola až se přejde za jeden config s bitversion v root
			$version['threadsafe']?"ts":"nts",
		])."/php.exe";

		if (!is_file($file)) {
			$apache = $this->apache.implode("/", [
				$version['version'],
				$version['subversion'],
			])."/php.conf";
			$root = $this->rootdir."php.conf";

			echo "PHP exe file for current version does not exists! PHP is not properly installed or config file is invalid.\n";
			echo "If you have created custom php config file in root then delete them or fix configuration.\n";
			echo "\tPHP: ".$file."\n";
			echo "\tCurrent Apache php config: ".$apache."\n";
			echo "\tCustom Apache php config: ".$root."\n";
			return null;
		}

		echo $file;
		if (count($params)) {
			echo " ".implode(" ", $params);
		}
		return $file;
	}
}
