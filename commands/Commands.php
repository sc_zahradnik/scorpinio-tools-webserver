<?php
namespace WebServer;

class Commands{
	protected $dir = "";
	protected $rootdir = "";
	protected $apache = "";
	protected $apacheTemplate = "";
	protected $php = "";
	protected $phpTemplate = "";

	protected $phpCommands;
	protected $apacheCommands;

	const BITVERSIONS = [64, 86];

	public function __construct(){
		$this->dir = __DIR__.'\\';
		$this->rootdir = str_replace("\\commands", "", $this->dir);
		$this->apache = $this->rootdir."apache\\";
		$this->php = $this->rootdir."php\\";
		$this->apacheTemplate = $this->dir."templates\\apache\\";
		$this->phpTemplate = $this->dir."templates\\php\\";
	}

	public function resolve(string $command = null, array $params = []){
		//todo: (0) asi dodělat redirect stejně jako u php
		if (is_null($command)) {
			echo "Command parametr cannot be empty!\n";
			return;
		}
		if (!method_exists($this, $command)) {
			echo 'Command "'.$command."\" does not exists!\n";
			return;
		}

		$prepCommand = "prep".ucfirst($command)."Params";
		if (method_exists($this, $prepCommand)) {
			$params = $this->{$prepCommand}($params);
		}
		// var_dump($command, $params);
		// exit;

		call_user_func_array([$this, $command], $params);
	}

	private function getCommandsClsInstance(string $property, string $cls): self{
		if (!($this->$property instanceof $cls)) {
			$this->$property = new $cls;
		}
		return $this->$property;
	}
	protected function getPhpCommands(): Php{
		return $this->getCommandsClsInstance("phpCommands", "WebServer\Php");
	}
	protected function getApacheCommands(): Apache{
		return $this->getCommandsClsInstance("apacheCommands", "WebServer\Apache");
	}

	protected function getSystemBitVersion(): ?int{
		exec("wmic os get osarchitecture", $output, $code);
		if ($output) {
			if (preg_match("/^(32|64|86)(\s?bit)?/m", implode("\n", $output), $matches)) {
				$bitversion = (int)$matches[1];
				if ($bitversion == 32) {
					$bitversion = 86;
				}
				return $bitversion;
			}
		}
		return null;
	}

	protected function getFlagParam(&$params, $flag){
		$param = null;
		if (($index = array_search($flag, $params)) !== false) {
			unset($params[$index]);
			$index++;
			$param = $params[$index];
			unset($params[$index]);
			$params = array_values($params);
		}

		return $param;
	}
}
