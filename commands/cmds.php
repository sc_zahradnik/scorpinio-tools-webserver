<?php
namespace WebServer;

$params = $argv;

$file = strtolower($params[0]);
$type = strtolower($params[1]);

ini_set('xdebug.var_display_max_depth', -1);
ini_set('xdebug.var_display_max_children', -1);
ini_set('xdebug.var_display_max_data', -1);

include("classes/PhpExeFilterIterator.php");
include("classes/ApacheExeFilterIterator.php");
include("Commands.php");
include("Apache.php");
include("Php.php");

switch ($type) {
	case 'apache':
		$commands = new Apache;
	break;
	case 'php':
		$commands = new Php;
	break;
	default:
		$commands = new Commands;
	break;
}

$command = null;
if (isset($params[2])) {
	$command = strtolower($params[2]);
}

if (method_exists($commands, $command)) {
	unset($params[2]);
} else {
	$command = $params[1];
}
unset($params[0]);
unset($params[1]);
$params = array_values($params);

$commands->resolve($command, $params);
