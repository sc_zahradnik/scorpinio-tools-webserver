@echo off

set bitver=0
wmic os get osarchitecture | cmd /c %~dp0grep.exe -oP -m1 "(64|32|86)">bitver.txt
set /P bitver= <bitver.txt
rm bitver.txt

if "%bitver%" NEQ "64" (
	if "%bitver%" NEQ "86" (
		set bitver=86
	)
)

echo %bitver%
exit
