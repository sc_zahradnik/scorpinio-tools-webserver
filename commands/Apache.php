<?php
namespace WebServer;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

use WebServer\classes\ApacheExeFilterIterator;

final class Apache extends Commands{
	const BATCHFILES = [
		"install",
		"uninstall",
		"start",
		"restart",
		"stop",
	];

	const REGEX_APACHE_VERSION_PATH = "/apache[\/\\\\](\d{1,3}\.\d{1,3})[\/\\\\](\d{1,3})[\/\\\\]x(64|86)/i";
	const REGEX_APACHE_FULLVERSION = "/((\d{1,3})\.(\d{1,3}))\.(\d{1,3})x(64|86)/i";

	//todo: (0) načtení verze z root config souboru
	private function getApacheVersion(): ?string{
		$version = null;
		exec("dir /b /ad ".$this->rootdir."\apache", $output, $code);
		if (is_array($output) && count($output)) {
			$version = $output[(count($output) - 1)];
		}
		return $version;
	}
	private function getApacheSubVersion($version): ?string{
		$subversion = null;
		exec("dir /b /ad ".$this->rootdir."\apache\\".$version, $output, $code);
		if (is_array($output) && count($output)) {
			$subversion = $output[(count($output) - 1)];
		}
		return $subversion;
	}

	public function getFullversion($version, $subversion, $bitversion = null, $threadsafe = true){
		$bitversion = $bitversion ?? $this->getSystemBitVersion();

		return $version.".".$subversion."x".$bitversion;//."-".($fcgi?"t":"f");//todo: (0) dořešit co s fcgi
	}
	public function parseFullversion(string $version){
		if (preg_match(self::REGEX_PHP_FULLVERSION, $version, $matches)) {
			return [
				'version'    => $matches[1],
				'subversion' => $matches[4],
				'bitversion' => $matches[5],
				'fcgi'       => true,//($matches[6]==="t"),//todo: (0) dořešit co s fcgi
			];
		}
		return null;
	}

	private function getVersions($version = null, $subversion = null, $bitversion = null){
		if (is_null($version)) {
			exec("dir /b /ad ".$this->rootdir."\apache", $output, $code);
			if (is_array($output) && count($output)) {
				$version = $output[(count($output) - 1)];
			}
		}
		if (!$version) {
			echo "Unknown version! Use param -v to set apache version.\n";
			echo "\tExample: apache start -v 2.4\n";
			return;
		}
		if (is_null($subversion)) {
			exec("dir /b /ad ".$this->rootdir."\apache\\".$version, $output, $code);
			if (is_array($output) && count($output)) {
				$subversion = $output[(count($output) - 1)];
			}
		}
		if (!$subversion) {
			echo "Unknown subversion! Use param -s to set apache subversion.\n";
			echo "\tExample: apache start -v 2.4 -s 46\n";
			echo "\t         apache start -s 46\n";
			return;
		}
		if (is_null($bitversion)) {
			exec("wmic os get osarchitecture", $output, $code);
			if ($output) {
				if (preg_match("/^(32|64|86)(\s?bit)?/m", implode("\n", $output), $matches)) {
					$bitversion = (int)$matches[1];
					if ($bitversion == 32) {
						$bitversion = 86;
					}
				}
			}
		}
		if (!$bitversion) {
			echo "Unknown system bit version! Use param -b to set bit version.\n";
			echo "\tExample: apache start -v 2.4 -s 46 -b 64\n";
			echo "\t         apache start -b 64\n";
			return;
		}

		return [
			$version,
			$subversion,
			$bitversion,
		];
	}
	private function getVersionParams(&$params){
		$version = null;
		$subversion = null;
		$bitversion = null;

		foreach (["-v" => "version", "-s" => "subversion", "-b" => "bitversion"] as $flag => $variable) {
			if (($index = array_search($flag, $params)) !== false) {
				unset($params[$index]);
				$index++;
				$$variable = $params[$index];
				unset($params[$index]);
				$params = array_values($params);
			}
		}

		return [
			$version,
			$subversion,
			$bitversion,
			$params,
		];
	}
	private function getNewApacheParams(&$params){
		$apache = null;
		$php = null;
		$oci = null;

		foreach (["-a" => "apache", "-p" => "php", "-o" => "oci"] as $flag => $variable) {
			$$variable = $this->getFlagParam($params, $flag);
		}

		return [
			$apache,
			$php,
			$oci,
		];
	}
	private function getApacheVersionParam(&$params){
		return $this->getFlagParam($params, "-a");
	}
	private function getPhpVersionParam(&$params){
		return $this->getFlagParam($params, "-p");
	}
	private function getVersionFromPaths(array $paths): array{
		$versions = [];
		foreach ($paths as $path) {
			$versions[] = $this->getVersionFromPath($path);
		}
		return $versions;
	}
	private function getVersionFromPath(string $path): ?array{
		if (preg_match(self::REGEX_APACHE_VERSION_PATH, $path, $matches)) {
			return [
				'version'    => $matches[1],
				'subversion' => $matches[2],
				'bitversion' => $matches[3],
				'fcgi'       => true,//todo: (0) získat z nějakého configu nebo tak něco
			];
		}
		return null;
	}

	private function parseApacheVersion($apache): ?array{
		if (preg_match("/(\d{1,2}\.\d{1,2})\.(\d{1,3})/", $apache, $matches)) {
			return [
				'version'    => $matches[1],//version
				'subversion' => $matches[2],//subversion
			];
		}
		return null;
	}
	private function parsePhpVersion($php): ?array{
		if (preg_match("/(\d{1,2}\.\d{1,2})\.(\d{1,3})/", $php, $matches)) {
			return [
				'version'    => $matches[1],//version
				'subversion' => $matches[2],//subversion
			];
		}
		return null;
	}

	public function getCurrentVersion(): ?array{
		$file = $this->rootdir."apache.conf";
		if (!is_file($file)) {
			$versions = $this->_list();
			$versions = array_reverse($versions);
			$keys = array_keys($versions);

			$pattern = $keys[0];
			$pattern = preg_replace("/x(64|86)/", "x(64|86)", $pattern);
			$pattern = str_replace(".", "\.", $pattern);

			$x86 = null;
			$x64 = null;
			foreach ($versions as $fullversion => $version) {
				if (is_null($x86) || is_null($x64)) {
					if (preg_match("/".$pattern."/", $fullversion, $matches)) {
						${"x".$matches[1]} = $fullversion;
					}
				} else {
					break;
				}
			}

			$bitversion = $this->getSystemBitVersion();
			$fullversion = ${"x".$bitversion};

			$file = $this->apache.implode("/", [
				$versions[$fullversion]['version'],
				$versions[$fullversion]['subversion'],
			])."/apache.conf";
		}

		if (!is_file($file)) {
			echo "Missing Apache config file! Custom file and file for latest Apache does not exists\n";
			echo "\tFile: ".$file."\n";
			echo "\tCustom file: ".$this->rootdir."apache.conf"."\n";
			return null;
		}
		$data = file_get_contents($file);
		$version = $this->parseApacheVersionFromConfigFile($data);
		return $version;

	}
	private function parseApacheVersionFromConfigFile(string $data): ?array{
		if (!preg_match("/^Define\sVERSION\s(\d{1,3}\.\d{1,3}).*$/mi", $data, $matches)){
			return null;
		}
		$version = $matches[1];

		if (!preg_match("/^Define\sSUBVERSION\s(\d{1,3}).*$/mi", $data, $matches)){
			return null;
		}
		$subversion = $matches[1];

		// if (!preg_match("/^Define\sBITVERSION\s(\d{1,3}).*$/mi", $data, $matches)){
		// 	return null;
		// }
		// $bitversion = $matches[1];
		//todo: (0) odebrat po vyřešení nastavení bitové verze přes root conf
		$bitversion = $this->getSystemBitVersion();

		$fcgi = false;
		if (preg_match("/^Define\sAPACHE_USE_FCGI\strue.*$/mi", $data, $matches)){
			$fcgi = true;
		}

		return [
			'version'    => $version,
			'subversion' => $subversion,
			'bitversion' => $bitversion,
			'fcgi'       => $fcgi,
		];
	}
	public function getCurrentPhpVersion(): ?array{
		$file = $this->rootdir."php.conf";

		if (!is_file($file)) {
			$apacheVersion = $this->getCurrentVersion();

			$file = $this->apache.implode("/", [
				$apacheVersion['version'],
				$apacheVersion['subversion'],
			])."/php.conf";

			if (!is_file($file)) {
				echo "Missing Php config file! Custom file and file for latest Apache does not exists\n";
				echo "\tFile: ".$file."\n";
				echo "\tCustom file: ".$this->rootdir."php.conf"."\n";
				return null;
			}
		}

		$data = file_get_contents($file);
		$version = $this->parsePhpVersionFromConfigFile($data);
		return $version;
	}
	private function parsePhpVersionFromConfigFile(string $data): ?array{
		if (!preg_match("/^Define\sPHP_VERSION\s(\d{1,3}\.\d{1,3}).*$/mi", $data, $matches)){
			return null;
		}
		$version = $matches[1];

		if (!preg_match("/^Define\sPHP_SUBVERSION\s(\d{1,3}).*$/mi", $data, $matches)){
			return null;
		}
		$subversion = $matches[1];

		// if (!preg_match("/^Define\sPHP_BITVERSION\s(\d{1,3}).*$/mi", $data, $matches)){
		// 	return null;
		// }
		$bitversion = $matches[1];
		//todo: (0) odebrat po vyřešení nastavení bitové verze přes root conf
		$bitversion = $this->getSystemBitVersion();

		if (!preg_match("/^Define\sPHP_MODULE_NAME\s([\w\d]*).*$/mi", $data, $matches)){
			return null;
		}
		$module = $matches[1];

		if (!preg_match("/^Define\sPHP_DLL_FILE\s([\w\d]*).*$/mi", $data, $matches)){
			return null;
		}
		$dll = $matches[1];

		$threadsafe = false;
		if (preg_match("/^Define\sPHP_THREAD_SAFE\strue.*$/mi", $data, $matches)){
			$threadsafe = true;
		}

		return [
			'version'    => $version,
			'subversion' => $subversion,
			'bitversion' => $bitversion,
			'threadsafe' => $threadsafe,
			'module'     => $module,
			'dll'        => $dll,
		];
	}

	///////////////////////
	// Create new Apache //
	///////////////////////
	public function prepCreateNewVersionParams($params){
		$tmp = $this->getNewApacheParams($params);
		$tmp[] = $params;
		return $tmp;
	}
	//todo: (0) načtení php a oci verze z adresářů
	public function createNewVersion($apache = null, $php = null, $oci = null, $params = []){
		$apache = $this->parseApacheVersion($apache);
		$php = $this->parsePhpVersion($php);

		$error = false;
		if (is_null($apache)) {
			$error = true;
			echo "Unknown Apache version! Version needs to be in format: [1-99].[1-99].[1-999]\n";
		}

		if (is_null($php)) {
			$error = true;
			echo "Unknown PHP version! Version needs to be in format: [1-99].[1-99].[1-999]\n";
		}

		if (!preg_match("/(\d{1,2}\.\d{1,2})/", $oci)) {
			$error = true;
			echo "Unknown OCI version! Version needs to be in format: [1-99].[1-99]\n";
		}

		if ($error) {
			return;
		}

		$destination = $this->apache.implode("\\", [$apache['version'], $apache['subversion']])."\\";
		if (file_exists($destination)) {
			echo "New version directory cannot be created because version directory already exists!\n";
			echo "Delete version dir or copy template files manualy from source directory.\n";
			echo "\tSource dir: ".$this->apacheTemplate."\n";
			echo "\tDestination dir: ".$destination."\n";
			return;
		}

		echo "Creating Apache ".implode(".", $apache)." version directory structure.\n";
		echo "Copying template files and directories.\n";
		exec("xcopy /E /I /Q /K ".trim(trim($this->apacheTemplate, "\\"),"/")." ".trim(trim($destination, "\\"),"/"), $output, $code);

		//todo: (0) převod na metody a použít i v commandu configure poud budou zadané parametry s verzí
		echo "Editing template config files.\n";
		$file = $destination."apache.conf";
		$data = file_get_contents($file);
		$data = preg_replace("/\{\{version\}\}/i", $apache['version'], $data);
		$data = preg_replace("/\{\{subversion\}\}/i", $apache['subversion'], $data);
		$data = preg_replace("/\{\{bitversion\}\}/i", 64, $data);
		file_put_contents($file, $data);

		$file = $destination."php.conf";
		$data = file_get_contents($file);
		$data = preg_replace("/\{\{version\}\}/i", $php['version'], $data);
		$data = preg_replace("/\{\{subversion\}\}/i", $php['subversion'], $data);
		$data = preg_replace("/\{\{bitversion\}\}/i", 64, $data);
		$data = preg_replace("/\{\{module\}\}/i", $phpModuleName??"php_module", $data);
		$data = preg_replace("/\{\{dllfile\}\}/i", $phpDllFile??"php".preg_replace("/(^\d)+\..*/", "$1", $php['version'])."apache".str_replace(".", "_", $apache['version']), $data);
		file_put_contents($file, $data);

		$file = $destination."oci.conf";
		$data = file_get_contents($file);
		$data = preg_replace("/\{\{version\}\}/i", $oci, $data);
		$data = preg_replace("/\{\{bitversion\}\}/i", 64, $data);
		file_put_contents($file, $data);

		echo "Copy manualy Apache files and directories into bit version directories.\n";
		echo "\tDirectories:\n";
		echo "\t\t".$destination."x64\\\n";
		echo "\t\t".$destination."x86\\\n";

		return;
	}

	///////////////
	// Configure //
	///////////////
	public function prepConfigureParams($params){
		$apache = $this->getApacheVersionParam($params);
		$php = $this->getPhpVersionParam($params);

		if (is_null($apache)) {
			$apache = $this->getApacheVersion();
			if (!is_null($apache)) {
				$apache .= ".".$this->getApacheSubversion($apache);
			}
		}
		if (is_null($apache)) {
			echo "Missing Apache version parameter!\n";
			echo "\tExample: -a 2.4.46\n";
		}

		$tmp = [
			$apache,
			$php,
			$params
		];
		return $tmp;
	}
	public function configure($apache, $php = null, $params = []){
		$apache = $this->parseApacheVersion($apache);
		$php = $this->parsePhpVersion($php);

		if (is_null($apache)) {
			$error = true;
			echo "Unknown Apache version! Version needs to be in format: [1-99].[1-99].[1-999]\n";
			return;
		}

		$pathVersion = $this->apache.implode("\\", [$apache['version'], $apache['subversion']])."\\";
		if (!file_exists($pathVersion)) {
			echo "Version directory does not exists!\n";
			echo "\tPath: ".$pathVersion."\n";
			return;
		}

		foreach (self::BITVERSIONS as $bitversion) {
			echo "\n";
			echo "Configuring Apache ".implode(".", $apache)."x".$bitversion."\n";
			$path = $pathVersion."x".$bitversion."\\";
			if (!file_exists($path)) {
				echo "Bit version directory does not exists!\n";
				echo "Use command \"apache createNewVersion\" with all version (apache, php, oci) params to create new version directories.\n";
				echo "\tPath: ".$path."\n";
				exit;
			}
			$this->configureApacheBitVersionConfig($path);
			$this->configureApacheBitVersionBatchfiles($path, $bitversion, implode(".", $apache));
		}

		return;
	}
	private function configureApacheBitVersionConfig($path){
		echo "Editing Apache config file.\n";
		$file = $path."conf\\httpd.conf";
		if (!is_file($file)) {
			echo "Bit version Apache config file does not exists!\n";
			echo "Please copy Apache files and directories into bit version directory.\n";
			echo "\tPath: ".$path."\n";
			echo "\tFile: ".$file."\n";
			exit;
		}

		$edited = false;
		$data = file_get_contents($file);
		if (!preg_match("/Include ..\/..\/..\/..\/root.conf/i", $data)) {
			$rep = "Include ../../../../root.conf\nInclude ../apache.conf\nDefine SRVROOT \"\${ROOT}apache/\${VERSION}/\${SUBVERSION}/x\${BITVERSION}\"";
			$data = preg_replace("/Define\sSRVROOT.*/i", $rep, $data);
			$edited = true;
		}
		if (!preg_match("/<IfModule dir_module>(\r\n)?.*DirectoryIndex.*index.php.*/im", $data)) {
			$data = preg_replace("/(<IfModule dir_module>(\r\n)?.*DirectoryIndex[^\r\n]*)/im", "$1 index.php", $data);
			$edited = true;
		}
		if (!preg_match("/Include\s\\$\{ROOT\}apache\/httpd.conf/i", $data)) {
			$data .= "\nInclude \${ROOT}apache/httpd.conf\n";
			$edited = true;
		}
		if ($edited) {
			file_put_contents($file, $data);
			echo "Apache bit version config file was edited.\n";
			echo "\tFile: ".$file."\n";
		}

		return;
	}
	private function configureApacheBitVersionBatchfiles($path, int $bitversion, string $apacheVersion){
		echo "Editing Apache batch files.\n";
		$edited = false;
		foreach (self::BATCHFILES as $filename) {
			$file = $path.$filename.".bat";
			if (!is_file($file)) {
				echo "Apache bit version batch file \"".$filename."\" does not exists!\n";
				echo "Please copy all template Apache batch files into bit version directory.\n";
				echo "\tPath: ".$path."\n";
				echo "\tFile: ".$file."\n";
				echo "\tTemplate file: ".$this->apacheTemplate."x".$bitversion."\\".$filename.".bat"."\n";
				exit;
			}

			$data = file_get_contents($file);
			$pattern = "/\{\{fullversion\}\}/i";
			if (preg_match($pattern, $data)) {
				$data = preg_replace($pattern, $apacheVersion."x".$bitversion, $data);
				file_put_contents($file, $data);
				$edited = true;
			}
		}

		if ($edited) {
			echo "Apache bit version batch files was edited.\n";
		}

		return;
	}

	/////////////
	// Install //
	/////////////
	public function prepInstallParams($params){
		$tmp = $this->getVersionParams($params);
		$tmp[] = $params;
		return $tmp;
	}
	public function install($version = null, $subversion = null, $bitversion = null, $params = []){
		[$version, $subversion, $bitversion] = $this->getVersions($version, $subversion, $bitversion);

		$apache = $this->apache.implode("\\", [$version,$subversion,"x".$bitversion])."\\";
		if (!file_exists($apache)) {
			echo "Apache dir for version ".$version.".".$subversion."x".$bitversion." does not exists!\n";
			echo "\tpath: ".$apache."\n";
			return;
		}

		echo $apache."install.bat";
		if (count($params)) {
			echo " ".implode(" ", $params);
		}
		return $apache;
	}

	///////////////
	// Uninstall //
	///////////////
	public function prepUninstallParams($params){
		$tmp = $this->getVersionParams($params);
		$tmp[] = $params;
		return $tmp;
	}
	public function uninstall($version = null, $subversion = null, $bitversion = null, $params = []){
		[$version, $subversion, $bitversion] = $this->getVersions($version, $subversion, $bitversion);

		$apache = $this->apache.implode("\\", [$version,$subversion,"x".$bitversion])."\\";
		if (!file_exists($apache)) {
			echo "Apache dir for version ".$version.".".$subversion."x".$bitversion." does not exists!\n";
			echo "\tpath: ".$apache."\n";
			return;
		}

		echo $apache."uninstall.bat";
		if (count($params)) {
			echo " ".implode(" ", $params);
		}
		return $apache;
	}

	///////////
	// Start //
	///////////
	public function prepStartParams($params){
		$tmp = $this->getVersionParams($params);
		$tmp[] = $params;
		return $tmp;
	}
	public function start($version = null, $subversion = null, $bitversion = null, $params = []){
		[$version, $subversion, $bitversion] = $this->getVersions($version, $subversion, $bitversion);

		$apache = $this->apache.implode("\\", [$version,$subversion,"x".$bitversion])."\\";
		if (!file_exists($apache)) {
			echo "Apache dir for version ".$version.".".$subversion."x".$bitversion." does not exists!\n";
			echo "\tpath: ".$apache."\n";
			return;
		}

		echo $apache."start.bat";
		if (count($params)) {
			echo " ".implode(" ", $params);
		}
		return $apache;
	}

	//////////
	// Stop //
	//////////
	public function prepStopParams($params){
		$tmp = $this->getVersionParams($params);
		$tmp[] = $params;
		return $tmp;
	}
	public function stop($version = null, $subversion = null, $bitversion = null, $params = []){
		[$version, $subversion, $bitversion] = $this->getVersions($version, $subversion, $bitversion);

		$apache = $this->apache.implode("\\", [$version,$subversion,"x".$bitversion])."\\";
		if (!file_exists($apache)) {
			echo "Apache dir for version ".$version.".".$subversion."x".$bitversion." does not exists!\n";
			echo "\tpath: ".$apache."\n";
			return;
		}

		echo $apache."stop.bat";
		if (count($params)) {
			echo " ".implode(" ", $params);
		}
		return $apache;
	}

	/////////////
	// Restart //
	/////////////
	public function prepRestartParams($params){
		$tmp = $this->getVersionParams($params);
		$tmp[] = $params;
		return $tmp;
	}
	public function restart($version = null, $subversion = null, $bitversion = null, $params = []){
		[$version, $subversion, $bitversion] = $this->getVersions($version, $subversion, $bitversion);

		$apache = $this->apache.implode("\\", [$version,$subversion,"x".$bitversion])."\\";
		if (!file_exists($apache)) {
			echo "Apache dir for version ".$version.".".$subversion."x".$bitversion." does not exists!\n";
			echo "\tpath: ".$apache."\n";
			return;
		}

		echo $apache."restart.bat";
		if (count($params)) {
			echo " ".implode(" ", $params);
		}
		return $apache;
	}


	///////////////////////
	// Open httpd config //
	///////////////////////
	public function prepConfigParams($params){
		$tmp = $this->getVersionParams($params);
		$tmp[] = $params;
		return $tmp;
	}
	public function config($version = null, $subversion = null, $bitversion = null, $params = []){
		[$version, $subversion, $bitversion] = $this->getVersions($version, $subversion, $bitversion);
		$apache = $this->apache.implode("\\", [$version,$subversion,"x".$bitversion])."\\";
		$apacheConfig = $apache."conf\\httpd.conf";

		// echo $apacheConfig;
		exec($apacheConfig, $output, $code);
	}

	///////////////////////
	// Open vhost config //
	///////////////////////
	public function prepVhostParams($params){
		$tmp = $this->getVersionParams($params);
		$tmp[] = $params;
		return $tmp;
	}
	public function vhost($version = null, $subversion = null, $bitversion = null, $params = []){
		[$version, $subversion, $bitversion] = $this->getVersions($version, $subversion, $bitversion);
		$apache = $this->apache.implode("\\", [$version,$subversion,"x".$bitversion])."\\";
		$apacheConfig = $apache."conf\\extra\\httpd-vhosts.conf";

		// echo $apacheConfig;
		exec($apacheConfig, $output, $code);
	}

	////////////////
	// Change PHP //
	////////////////
	public function prepChangePhpParams($params){
		$php = $this->getPhpCommands();
		$tmp[] = $php->getFullversionParam($params);
		$tmp = array_merge($tmp, $php->getVersionParams($params));
		return $tmp;
	}
	public function changePhp($fullversion = null, $version = null, $subversion = null, $bitversion = null, $threadsafe = true, $params = []){
		$php = $this->getPhpCommands();

		$expression = !is_null($version) && !is_null($subversion);

		$bitversion = $bitversion ?? $this->getSystemBitVersion();

		if (is_null($fullversion) && !$expression) {
			echo "Missing version params! Specify PHP version. Use -p parameter or set of version parameters.\n";
			$php->echoFullversionParam();
			echo "\tor\n";
			$php->echoSetOfVersionParams();
			echo "\n";
			return;
		}

		$versions = $php->_list();

		if (is_null($fullversion)) {
			$fullversion = $php->getFullversion($version, $subversion, $bitversion, $threadsafe);
		} else {
			$version = $php->parseFullversion($fullversion);
			if ($version) {
				[$version, $subversion, $bitversion, $threadsafe] = array_values($version);
			}
		}

		if (!array_key_exists($fullversion, $versions)) {
			echo "Php version ".$fullversion." is not installed or version string is not valid! Use -p parameter or set of version parameters.\n";
			echo "\tExample for -p param: 8.0.1x64-ts\n";
			echo "\tOr you can use set of params -v -s -b- t\n";
			$php->echoSetOfVersionParams();
			echo "\n";
			return;
		}

		$module = $this->getFlagParam($params, "-m");
		$dll = $this->getFlagParam($params, "-d");

		if ($this->createCustomPhpConfig($version, $subversion, $bitversion, $threadsafe, $module, $dll)) {
			// todo: (0) cmd kontrola zda existuje service a zda běží a podle toho upravit zprávu
			echo "Php version was changed. Running Apache needs to be restarted.\n";
		} else {
			echo "Php version config probably was not created! You can make it manualy from config template. Custom config file needs to be in root directory.\n";
			echo "\tPath: ".$this->apacheTemplate."php.conf"."\n";
			echo "\tPath root: ".$this->rootdir."\n";
		}

		echo "\n";
		return;
	}

	public function createCustomPhpConfig($version, $subversion, $bitversion = null, $threadsafe = true, $module = null, $dll = null){
		$bitversion = $bitversion ?? $this->getSystemBitVersion();

		$data = file_get_contents($this->apacheTemplate."php.conf");

		$data = $this->replacePhpConfValues($data, $version, $subversion, $bitversion, $threadsafe, $module, $dll);

		return file_put_contents($this->rootdir."php.conf", $data);
	}
	public function replacePhpConfValues(string $data, $version, $subversion, $bitversion, $threadsafe = true, $module = null, $dll = null){
		//todo: (0) dořešit co s verzí apache, spíš asi $php->getApacheDllFile($version, $subversion, $bitversion);//threadsafe není potřeba protože apache dll v nts není, takže hledat jen v ts
		$dll = $dll ?? "php".preg_replace("/(^\d)+\..*/", "$1", $version)."apache2_4";
		// $dll = $dll ?? "php".preg_replace("/(^\d)+\..*/", "$1", $version)."apache".str_replace(".", "_", $apache['version']);

		//todo: (0) načtení module z adresáře, pravděpodobně při instalaci php bude potřeba vytvořit soubor ve kterém bude tento název 
		$module = $module ?? "php_module";

		$data = preg_replace("/\{\{version\}\}/i", $version, $data);
		$data = preg_replace("/\{\{subversion\}\}/i", $subversion, $data);
		$data = preg_replace("/\{\{bitversion\}\}/i", $bitversion, $data);
		$data = preg_replace("/\{\{module\}\}/i", $module, $data);
		$data = preg_replace("/\{\{dllfile\}\}/i", $dll, $data);

		if ($threadsafe) {
			$data = preg_replace("/#(Define\sPHP_THREAD_SAFE\strue)/i", "$1", $data);
		}

		return $data;
	}


	//////////
	// List //
	//////////
	public function _list(){
		$rdi = new RecursiveDirectoryIterator($this->apache);
		$mfi = new ApacheExeFilterIterator($rdi);
		$rii = new RecursiveIteratorIterator($mfi);

		$versions = [];
		$rii->rewind();
		foreach ($rii as $file) {
			if ($file->isFile()) {
				$version = $this->getVersionFromPath($file->getPath());
				if ($version) {
					$fullversion = $this->getFullversion(...array_values($version));
					if (!array_key_exists($fullversion, $versions)) {
						$versions[$fullversion] = $version;
					}
				}
			}
		}
		return $versions;
	}
	public function list(){
		$versions = $this->_list();

		echo implode("\n", array_keys($versions));
		echo "\n";
	}
}
