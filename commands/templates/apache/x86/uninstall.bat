@echo off

set flag=%1

set name=Apache{{fullversion}}

IF "%flag%" == "-t" (
	set name=%name%_t
)

sc queryex type=service state=all | cmd /c %~dp0../../../../commands/grep.exe -oP "%name%\s">tmp.txt
set /P tmp= < tmp.txt
set tmp=%tmp%
rm tmp.txt

IF "%tmp%" EQU "%name%" (
	%~dp0bin/httpd.exe -k uninstall -n %name%
)
