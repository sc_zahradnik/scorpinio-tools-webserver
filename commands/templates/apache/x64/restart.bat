@echo off

set tmp=0
set flag=%1

set name=Apache{{fullversion}}

sc queryex type=service | cmd /c %~dp0../../../../commands/grep.exe -oP -m1 "%name%(_t)?\s">tmp.txt
set /p tmp= < tmp.txt
set tmp=%tmp%
rm tmp.txt

echo %tmp% | cmd /c %~dp0../../../../commands/grep.exe -oP "_t">tmp2.txt
set /p tmp2= < tmp2.txt
rm tmp2.txt

IF "%tmp2%" NEQ "" (
	set flag=-t
)
IF "%flag%" EQU "-t" (
	echo Restarting temporary service %name%_t

	rem terporary service
	rem system will remove it after stoping service
	rem runas /profile /user:Martin "cmd /c %~dp0bin/httpd.exe -k start -n %name%"
	cmd /c %~dp0stop.bat
	cmd /c %~dp0start.bat -t
) ELSE IF %tmp% EQU %name% (
	echo Restarting service %name%
	rem runas /profile /user:Martin "cmd /c %~dp0bin/httpd.exe -k start -n %name%"
	cmd /c %~dp0stop.bat
	cmd /c %~dp0start.bat
)
