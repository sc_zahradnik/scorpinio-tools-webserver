@echo off

set tmp=0

set name=Apache{{fullversion}}
set regex=%name%(_t)?\s

sc queryex type=service | cmd /c %~dp0../../../../commands/grep.exe -oP -m1 "%regex%">tmp.txt
set /p tmp= < tmp.txt
rm tmp.txt

IF %tmp% NEQ "" (
	echo Stopping service %tmp%
	rem runas /profile /user:Martin "cmd /c %~dp0bin/httpd.exe -k start -n %tmp%"
	cmd /c %~dp0bin/httpd.exe -k stop -n "%tmp%"
)
