<?php
namespace WebServer\classes;

use RecursiveFilterIterator;

class ApacheExeFilterIterator extends RecursiveFilterIterator{
    public function __construct($iterator){
        parent::__construct($iterator);
    }

    public function accept(){
        $pattern = "/httpd\.exe/ui";
        return $this->current()->isDir() xor ($this->current()->isFile() && preg_match($pattern, $this->getFilename()));
    }

    public function __toString(){
        return $this->current()->getFilename();
    }
}