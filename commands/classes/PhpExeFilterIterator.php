<?php
namespace WebServer\classes;

use RecursiveFilterIterator;

class PhpExeFilterIterator extends RecursiveFilterIterator{
    public function __construct($iterator){
        parent::__construct($iterator);
    }

    public function accept(){
        $pattern = "/php(-cgi)?\.exe/ui";
        return $this->current()->isDir() xor ($this->current()->isFile() && preg_match($pattern, $this->getFilename()));
    }

    public function __toString(){
        return $this->current()->getFilename();
    }
}