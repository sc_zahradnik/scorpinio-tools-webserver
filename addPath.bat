@ECHO off

set th=%~dp0
set th=%th:\=/%
set tmp=0

cmd /c %~dp0commands/getBitVersion.bat>tmp.txt
set /P bitversion= <tmp.txt
rm tmp.txt

cmd /c %~dp0commands/pathmanx%bitversion%.exe list | cmd /c %~dp0commands/grep.exe -oP "%th%">tmp.txt
set /P tmp= <tmp.txt
rm tmp.txt

IF "%th%" NEQ "%tmp%" (
	cmd /c %~dp0commands/pathmanx%bitversion%.exe add %th%
)