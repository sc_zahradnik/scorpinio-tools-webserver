@echo off

set batchfile=""

set bitVersion=86
cmd /c %~dp0commands/getBitVersion.bat>tmp.txt
set /P bitVersion= < tmp.txt

cmd /c %~dp0commands/php/%bitVersion%/php.exe %~dp0commands/cmds.php php %*>tmp.txt
rem goto :end
set /P batchfile= < tmp.txt

if "%1" EQU "list" goto :semiend

set tmp2=0
echo %batchfile% | findstr "php.exe">tmp2.txt
set /P tmp2= < tmp2.txt
rm tmp2.txt

if "%tmp2%" NEQ "0" (
	cmd /c %batchfile%
	goto :end
) else (
	:semiend
	type tmp.txt
	rm tmp.txt
)

:end
