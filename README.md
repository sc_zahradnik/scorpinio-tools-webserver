## How to setup
1. Open cmd as admin
2. Clone repository
3. Open cloned repository dir
4. Run setRoot.bat - Creates config file for Apache with root dir path.
5. Run addPath.bat - Adds root dir into user paths.
*Do not forget remove other php and apache paths.

Example:


    git clone git@bitbucket.org:sc_zahradnik/scorpinio-tools-webserver.git webserver
    cd webserver
    setRoot
    addPath

## Apache
After setup you are able to use apache commands in command line anywhere.
Any command starts with "apache". Does not matter if it is webserver tool command or apache (httpd.exe) command. Currently used version is every time latest version in webserver tool. You can specify which version you want to use by using version parameters.

Examples:


    apache -v
	apache start
    apache restart
    apache stop

### Commands
#### List
Returns all available Apache versions.
Usefull for commands where you need specify version.


    apache list
#### Install & Uninstall
Creates or removes service for current Apache version.
If you want to use Apache as service you have to run command line as admin to use commands like install, uninstall, start, restart, stop...
##### Parameters:
1. -v
	- Specify Apache version.
	- Format: -v[space][version]
	- "-v 2.4" For Apache2.4.46x64
2. -s
	- Specify Apache subversion.
	- Format: -s[space][subversion]
	- "-s 46" For Apache2.4.46x64
3. -b
	- Specify Apache bitversion.
	- Format: -b[space][version]
	- "-b 64" For Apache2.4.46x64 
	- Not required. Tool will recognize system bit version. Use only when you need run x86 on x64 system.
4. -f (feature, will be added)
	- Specify full Apache version.
	- Format: -f[space][version].[subversion]x[bitversion]
	- "-f Apache2.4.46x64" For Apache2.4.46x64 

Examples:
Creates service for latest version.


    apache install
Creates service for specific subversion.


    apache install -s 35
Removes service for latest version.


    apache uninstall
Removes service for specific subversion.


    apache uninstall -s 35

#### Start & Restart & Stop
Restart and stop commands are just for work with service.
All three commands accepts version params like prievious commands install and uninstall.
Restart will restart currently running service. So temporary service will be stopped (removed) and again created as temporary.
##### Parameters:
1. -t
	- Apache will be running as temporary service.
	- Service will be automaticaly removed after using stop command.

Examples:
Basic run. For stop Apache just close command line or use ctrl+c.


    apache start
Service run. Install command is required just once. Every time since "install" command you can use just "start" command. 


    apache install
    apache start
#### Config
Will open config file for current Apache version. Currently config file in Apache version directory (*conf\httpd.conf*) but you can create custom httpd.conf file in root directory. Root config file will be used in every Apache version.
todo: dopsat
#### Vhost
Will open vhost file for current Apache version. Currently vhost config file in Apache version directory (*conf\extra\httpd-vhosts.conf*) but in root directory exists vhost file too (vhosts.conf). Root vhost config will be used in every Apache version.
todo: dopsat
#### Change PHP
Will create custom Apache config file in root with all needed variables.
todo: dopsat

## PHP
### How to use
After setup (*setRoot & addPath*) you are able to use php commands in command line anywhere.
Any command starts with "php". Does not matter if it is webserver tool command or php (php.exe) command. Currently used version is every time latest version in webserver tool. You can specify which version you want to use by using version parameters.
### Commands
#### List
Returns all available PHP versions.
Usefull for commands where you need specify version like "*apache changePhp -f 8.0.2x64*"


    apache list
